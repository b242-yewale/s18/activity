function addNumbers(num1, num2){
	console.log("Result of summation of "+ num1 +" and "+ num2 +" is: "+(num1 + num2));
}


function substractNumbers(num1, num2){
	console.log("Result of substraction of "+ num1 +" and "+ num2 +" is: "+(num1 - num2));
}


addNumbers(2,3);

substractNumbers(2,3);


function multiplyNumbers(num1, num2){
	multiplyResult = num1 * num2;
	return multiplyResult;
}


function divideNumbers(num1, num2){
	divideResult = num1 / num2;
	return divideResult;
}


let product = multiplyNumbers(2,3);

let quotient = divideNumbers(50,10);

console.log("Result of multiplication of the numbers is: "+product);

console.log("Result of division of the numbers is: "+quotient);


function circlAreaCalculator(radius){
	let areaOfCircle = 3.14 * radius**2;
	return areaOfCircle;
}


let circleRadius = 15
let circleArea = circlAreaCalculator(circleRadius);

console.log("Area of circle of the radius "+circleRadius + " is "+circleArea +" units.");


function averageOf4(num1, num2, num3, num4){
	let average = (num1 + num2 + num3 + num4)/4;
	return average;
}


let averageVar = averageOf4(2,3,4,5);

console.log("The average of the four numbers is "+averageVar);


function passOrFailCheck(userScore, totalScore){
	let percentage = (userScore/totalScore)*100;
	let isPassed = percentage >= 75;
	return isPassed;
}


recievedScore = 74;
netScore = 100;
let isPassingScore = passOrFailCheck(recievedScore, netScore);

console.log("is "+ (recievedScore) + "/" + (netScore) + " a passing score?: "+isPassingScore);